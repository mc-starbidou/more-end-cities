package net.fabricmc.starbidou.moreendcities;

import net.fabricmc.api.ModInitializer;
import net.minecraft.structure.StructureSets;
import net.minecraft.world.gen.chunk.placement.RandomSpreadStructurePlacement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoreEndCities implements ModInitializer {

	public static final Logger LOGGER = LoggerFactory.getLogger("starbidous_more_end_cities");
	@Override
	public void onInitialize() {
		RandomSpreadStructurePlacement placement =  (RandomSpreadStructurePlacement)StructureSets.END_CITIES.value().placement();

		// Very simple, not configurable
		placement.spacing = 6;
		placement.separation = 3;
	}
}